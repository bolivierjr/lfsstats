import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addStats } from '../actions/index';

class StatsList extends Component {
  constructor(props) {
    super(props);
    this.state ={ tasks: ''}
  }

  onSubmit(e) {
    this.props.onAddStat(this.state.tasks);
    e.preventDefault();
    
    
  }

  onChange(e) {
    this.setState({
      tasks: e.target.value
    });
  }

  render() {
    return(
      <div>
        <form onSubmit={this.onSubmit.bind(this)} >
          <input placeholder="add user" onChange={this.onChange.bind(this)}/>
          <button type="submit">Enter</button>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { stats: state.stats };
}

export default connect(mapStateToProps, {addStats})(StatsList);
