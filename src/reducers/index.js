import { combineReducers } from 'redux';
import StatsReducer from './reducer_stats';


const rootReducer = combineReducers({
  stats: StatsReducer
});

export default rootReducer;
