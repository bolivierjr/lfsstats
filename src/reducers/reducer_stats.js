import { ADD_STATS, USER } from '../actions/index';


export default function(state = [], action) {
  switch(action.type) {

    case ADD_STATS:
      return { ...state, racer: USER, stat: action.payload.data};

    default:
      return state;
  }
}
