import React, { Component } from 'react';
import StatsList from '../containers/add_stats';
import axios from 'axios';

export default class App extends Component {
  constructor() {
    super();
    this.state = { stats: [], tasks: ''}
  }


  getStats(user) {
    console.log(user);
    axios.request({
      method: 'get',
      url: 'http://www.lfsworld.net/pubstat/get_stat2.php?version=1.5&action=pb&idk=7p33EdzYnwLn7RgyiSNe2sw3UxjZyIrf&s=1&racer='+user
    }).then((response) => {
      this.setState({stats: response.data}, () => {
        console.log(resonse.data);
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  render() {
    return (
      <div>
        <StatsList onAddStat={this.getStats.bind(this)} />
      </div>
    );
  }
}
